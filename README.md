# Delivery service


### High level design diagram ###
https://miro.com/app/board/uXjVPM3U2z8=/?share_link_id=270244355879

### Software development stack ###
- Postgresql 12.3
- Django 3.8.3 (Admin panel)
- Aiohttp 4.1.2 (API)

### URLs ###
admin panel: http://localhost:8000/admin/ \
API: http://localhost:8080/api/docs

#### Admin panel: ####
```
Login: admin
Password: admin_password
```

### Run project ###
Install **docker**: https://docs.docker.com/engine/install/ \
Install **docker-compose**: https://docs.docker.com/compose/install/
```commandline
docker-compose up --build -d
```
Command for filling the database with test data :
```commandline
cd admin_app/app
python manage.py fill_test_data
```

### Tests ###
Running tests, run when the database is running
```commandline
cd api_app/app
pytest -s tests
```