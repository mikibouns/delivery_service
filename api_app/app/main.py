"""main module"""
from aiohttp import web
from aiohttp_middlewares import cors_middleware
from aiohttp_swagger3 import SwaggerDocs, SwaggerUiSettings, SwaggerInfo

import views
from middlewares import middleware_handler
import config as cfg
from db.init_db import db_engine


def main():
    """
    Main function
    :return: application instance
    """
    app = web.Application(middlewares=[cors_middleware(
        allow_all=cfg.CORS_ALLOW_ALL, origins=cfg.CORS_ALLOW_ORIGINS
    ), middleware_handler])
    swagger = SwaggerDocs(
        app,
        swagger_ui_settings=SwaggerUiSettings(
            path="/api/docs/", withCredentials=True
        ),
        info=SwaggerInfo(
            title="Delivery service API", version=cfg.API_VERSION
        ),
        components="components.yml"
    )
    swagger.add_routes(views.routes)
    db_engine(app)
    app['storage'] = {}
    return app


async def run_app():
    """main runner"""
    return main()


if __name__ == "__main__":
    web.run_app(main(), host='0.0.0.0', port=cfg.APP_PORT)
