"""module for search couriers"""
from typing import List, Optional

from tortoise.models import Model
from loguru import logger

from db import models
from utils import helper_lib as hl


async def get_delivery_zone(point: List[float]) -> Optional[Model]:
    """
    Get delivery zone
    :param point: example: [55.858406154570794, 37.25130442578125]
    :return: delivery zone object
    """
    for dz_obj in await models.DeliveryZone.filter(is_active=True):
        area = dz_obj.coordinates
        if hl.is_point_in_area(point, area):
            return dz_obj


async def find_courier(
        point: List[float], delivery_zone_id: int
) -> Optional[Model]:
    """
    Find courier
    :param point: [55.858406154570794, 37.25130442578125]
    :param delivery_zone_id: 12
    :return: courier object
    """
    result = None
    min_distance = 0
    for courier_obj in await models.Courier.filter(
        delivery_zone_id=delivery_zone_id, is_active=True
    ):
        distance = hl.coordinate_distance_calculator(
            [point, courier_obj.coordinates]
        )
        logger.debug(f"{courier_obj}, {distance}")
        if min_distance == 0 or distance <= min_distance:
            min_distance = distance
            result = courier_obj
    return result
