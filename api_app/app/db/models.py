"""module for ORM models"""
from datetime import datetime
import json

from tortoise import fields, Model
from tortoise.transactions import in_transaction


class DeliveryZone(Model):
    """Delivery zone model"""
    name = fields.CharField(max_length=254, unique=True)
    coordinates = fields.JSONField()
    is_active = fields.BooleanField(default=True)

    courier: fields.ReverseRelation["Courier"]

    class Meta:
        """Meta class"""
        table = 'delivery_zone'

    def __str__(self):
        return f'{self.name}'


class Courier(Model):
    """Courier model"""
    first_name = fields.CharField(max_length=254, null=True)
    last_name = fields.CharField(max_length=254, null=True)
    email = fields.CharField(max_length=254, unique=True)
    phone = fields.CharField(max_length=128, null=True)
    is_active = fields.BooleanField(default=True)
    date_joined = fields.DatetimeField(default=datetime.now)
    coordinates = fields.JSONField(
        verbose_name='Coordinates', blank=True, null=True
    )
    delivery_zone: fields.ForeignKeyRelation[
        DeliveryZone] = fields.ForeignKeyField(
        'models.DeliveryZone', related_name='courier',
        on_delete=fields.SET_NULL, null=True
    )

    courier_order: fields.ReverseRelation["Order"]

    class Meta:
        """Meta class"""
        table = 'courier'
        ordering = ['date_joined']

    @staticmethod
    async def get_courier_list(ids: tuple = tuple()) -> list:
        """
        get_courier_list
        :param ids: courier ids
        :return: courier object list
        """
        table_courier = 'courier'
        table_delivery_zone = 'delivery_zone'
        request_data = f"""
        SELECT
            DISTINCT {table_courier}.id as courier_id,
            {table_courier}.first_name,
            {table_courier}.last_name,
            {table_courier}.email,
            {table_courier}.phone,
            {table_courier}.date_joined,
            {table_courier}.coordinates,
            {table_delivery_zone}.id as delivery_zone_id,
            {table_delivery_zone}.name

        FROM {table_courier}
        JOIN {table_delivery_zone} on {table_delivery_zone}.id = {table_courier}.delivery_zone_id
        WHERE {table_courier}.is_active = true 
        """
        if ids:
            if len(ids) > 1:
                request_data += f" AND {table_courier}.id IN {ids}"
            else:
                request_data += f" AND {table_courier}.id  = {ids[0]}"
        request_data += f" ORDER BY {table_courier}.id"

        async with in_transaction() as connection:
            data = await connection.execute_query(request_data)
            result = data[1]
        return [{
            "id": data['courier_id'],
            "first_name": data['first_name'],
            "last_name": data['last_name'],
            "email": data['email'],
            "phone": data['phone'],
            "date_joined": data['date_joined'].isoformat(),
            "coordinates": json.loads(data['coordinates']) if data[
                'coordinates'] else [],
            "delivery_zone": {
                "id": data['delivery_zone_id'],
                "name": data['name']
            }
        } for data in result]

    def __str__(self):
        return f'{self.email}'


class Client(Model):
    """Client model"""
    first_name = fields.CharField(max_length=254, null=True)
    last_name = fields.CharField(max_length=254, null=True)
    email = fields.CharField(max_length=254, unique=True)
    phone = fields.CharField(max_length=128, null=True)
    is_active = fields.BooleanField(default=True)
    date_joined = fields.DatetimeField(default=datetime.now)

    client_order: fields.ReverseRelation["Order"]

    class Meta:
        """Meta class"""
        table = 'client'
        ordering = ['date_joined']

    def __str__(self):
        return f'{self.email}'


class Order(Model):
    """Order model"""
    title = fields.CharField(max_length=254, null=True)
    description = fields.TextField(null=True)
    quantity = fields.IntField(default=0)
    price = fields.FloatField(default=0)
    address = fields.CharField(max_length=254, null=True)
    coordinates = fields.JSONField(null=True)
    status = fields.IntField(default=0)
    client: fields.ForeignKeyRelation[Client] = fields.ForeignKeyField(
        'models.Client', related_name='client_order',
        on_delete=fields.CASCADE, null=True
    )
    courier: fields.ForeignKeyRelation[Courier] = fields.ForeignKeyField(
        'models.Courier', related_name='courier_order',
        on_delete=fields.CASCADE, null=True
    )

    class Meta:
        """Meta class"""
        table = 'order'

    def __str__(self):
        return f'{self.title}'
