"""module for pydantic models"""
from typing import Optional, List

from pydantic import BaseModel, validator, EmailStr


class DeliveryZonesRequestModel(BaseModel):
    """Delivery zones request model"""
    name: str
    coordinates: list

    @validator('coordinates')
    def coordinates_validator(cls, value: List[float]) -> List[float]:
        """
        Coordinates validator
        :param value: coordinates data
        :return: coordinates data or exception
        """
        if len(value) < 3:
            raise ValueError('The number of elements must be greater than 3')
        return value


class CouriersRequestModel(BaseModel):
    """Couriers request model"""
    first_name: Optional[str]
    last_name: Optional[str]
    email: EmailStr
    phone: Optional[str]
    coordinates: Optional[List[float]]
    delivery_zone_id: int

    @validator('coordinates')
    def coordinates_validator(cls, value: List[float]) -> List[float]:
        """
        Coordinates validator
        :param value: coordinates data
        :return: coordinates data or exception
        """
        if len(value) != 2:
            raise ValueError('The number of elements should be 2')
        return value
