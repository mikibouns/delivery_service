"""module for database initialization"""
from tortoise.contrib.aiohttp import register_tortoise

import config as cfg


def db_engine(app):
    """
    DB connector
    :param app: application instance
    :return:
    """
    db_url = f"postgres://{cfg.DB_USER}:{cfg.DB_PASS}@" \
             f"{cfg.DB_HOST}:{cfg.DB_PORT}/{cfg.DB_NAME}"
    register_tortoise(
        app,
        db_url=db_url,
        modules={"models": ["db.models"]},
        generate_schemas=False
    )
