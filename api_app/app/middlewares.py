"""middlewares module"""
import json

from aiohttp import web
from aiohttp_swagger3 import RequestValidationFailed
from pydantic.error_wrappers import ValidationError

from utils import helper_lib as hl


@web.middleware
async def middleware_handler(request, handler):
    """
    Middleware handler
    :param request: http request
    :param handler: handler
    :return: http request
    """
    path_exception_pool = [['api', 'docs'], ]
    try:
        response = await handler(request)
        if request.path.split('/')[1:3] in path_exception_pool:
            return response

        return web.json_response({
            'success': True,
            'code': response.status,
            'message': None,
            'data': json.loads(response.text) if response.text else None
        }, status=response.status)
    except ValidationError as ex:
        message = await hl.formatted_pydantic_error(ex.errors())
        return web.json_response({
            'success': False,
            'code': 422,
            'message': {'body': message},
            'data': None
        }, status=422)
    except RequestValidationFailed as ex:
        try:
            message = json.loads(ex.reason)
            if message.get('authorization'):
                message = {'header': message}
        except ValueError:
            message = ex.reason
        return web.json_response({
            'success': False,
            'code': ex.status_code,
            'message': message,
            'data': None
        }, status=ex.status_code)
    except web.HTTPException as ex:
        try:
            message = json.loads(ex.reason)
        except ValueError:
            message = ex.reason
        return web.json_response({
            'success': False,
            'code': ex.status_code,
            'message': message,
            'data': None
        }, status=ex.status_code)
