"""testing module"""


async def test_get_delivery_zones(cli, delivery_zones):
    """
    Test get delivery zones
    :param cli: client
    :param delivery_zones: delivery zones
    :return:
    """
    test_response_data = [{
        "id": dz_obj.id,
        "name": dz_obj.name,
        "coordinates": dz_obj.coordinates
    } for dz_obj in delivery_zones]
    response = await cli.get("/api/delivery-zones")
    assert response.status == 200
    response = await response.json()
    assert response['data'] == test_response_data


async def test_set_delivery_zones(cli, delivery_zones):
    """
    Test set delivery zones
    :param cli: client
    :param delivery_zones: delivery zones
    :return:
    """
    test_request_data = [
        [{
            "name": "zone_4",
            "coordinates": [
                [55.8251794700682, 36.91484812695314],
                [55.8251794700682, 36.91484812695314],
                [55.8251794700682, 36.91484812695314]
            ]
        }],
        [{
            "name": "zone_5",
            "coordinates": [
                [55.8251794700682, 36.91484812695314],
                [55.8251794700682, 36.91484812695314]
            ]
        }],
        [{
            "name": "zone_4",
            "coordinates": [
                [55.8251794700682, 36.91484812695314],
                [55.8251794700682, 36.91484812695314],
                [55.8251794700682, 37.9148481269531]
            ]
        }],
        [{
            "coordinates": [
                [55.8251794700682, 36.91484812695314],
                [55.8251794700682, 36.91484812695314],
                [55.8251794700682, 37.9148481269531]
            ]
        }],
        [{"name": "zone_4"}],
        [{
            "name": 12,
            "coordinates": [
                [55.8251794700682, 36.91484812695314],
                [55.8251794700682, 36.91484812695314],
                [55.8251794700682, 37.9148481269531]
            ]
        }]
    ]
    test_response_data = [
        {
            "success": True,
            "code": 200,
            "message": None,
            "data": [{
                "id": delivery_zones[2].id + 1,
                "name": "zone_4",
                "coordinates": [
                    [55.8251794700682, 36.91484812695314],
                    [55.8251794700682, 36.91484812695314],
                    [55.8251794700682, 36.91484812695314]
                ]
            }]
        },
        {
            "success": False,
            "code": 422,
            "message": {"body": {
                "coordinates": "The number of elements must be greater than 3"
            }},
            "data": None
        },
        {
            "success": False,
            "code": 422,
            "message": "duplicate key value violates unique constraint"
                       " \"delivery_zone_name_key\"\nDETAIL:  Key (name)="
                       "(zone_4) already exists.",
            "data": None
        },
        {
            "success": False,
            "code": 400,
            "message": {"body": {"0": {"name": "required property"}}},
            "data": None
        },
        {
            "success": False,
            "code": 400,
            "message": {"body": {"0": {"coordinates": "required property"}}},
            "data": None
        },
        {
            "success": False,
            "code": 400,
            "message": {
                "body": {"0": {"name": "value should be type of str"}}},
            "data": None
        }
    ]
    for index, data in enumerate(test_request_data):
        response = await cli.post("/api/delivery-zones", json=data)
        assert response.status == test_response_data[index]['code']
        assert await response.json() == test_response_data[index]
