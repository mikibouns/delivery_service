"""testing module"""
from utils import helper_lib as hl


async def test_coordinate_distance_calculator():
    """
    Test coordinate distance calculator
    :return:
    """
    test_request_data = [
        [
            [55.858406154570794, 37.25130442578125],
            [55.91434461155746, 37.3198694030242]
        ],
        [
            [55.858406154570794, 37.25130442578125],
            [55.939017200935496, 37.247084979196096]
        ],
        [
            [55.858406154570794, 37.25130442578125],
            [55.89505817364596, 37.7098840514617]
        ]
    ]
    test_response_data = [
        7.550365135231191,
        8.970213437779668,
        28.902982640772176
    ]
    for index, data in enumerate(test_request_data):
        response = hl.coordinate_distance_calculator(data)
        assert response == test_response_data[index]


async def test_is_point_in_area():
    """
    Test is point in area
    :return:
    """
    test_request_data = [
        {
            "point": [55.858406154570794, 37.25130442578125],
            "area": [
                [55.8251794700682, 36.91484812695314],
                [55.79037681251229, 37.37078074414063],
                [55.91012114854278, 37.57402781445313],
                [56.05639958730052, 37.43532542187501],
                [56.03949052052108, 37.00823191601564],
                [55.8251794700682, 36.91484812695314]
            ],
        },
        {
            "point": [55.858406154570794, 37.25130442578125],
            "area": [
                [55.78923308820743, 37.36732885120619],
                [55.91013799830256, 37.576069085581175],
                [55.821330939602774, 37.83699437854993],
                [55.65277587544564, 37.841719988961714],
                [55.59371854351092, 37.510756854196096],
                [55.78923308820743, 37.36732885120619]
            ]
        }
    ]
    test_response_data = [True, False]
    for index, data in enumerate(test_request_data):
        response = hl.is_point_in_area(**data)
        assert response == test_response_data[index]
