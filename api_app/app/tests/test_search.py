"""testing module"""
from datetime import timedelta

from utils import helper_lib as hl


async def test_search_courier(cli, couriers):
    """
    Test search courier
    :param cli: client
    :param couriers: couriers
    :return:
    """
    offset = hl.get_timezone_offset()
    test_request_data = [
        [55.858406154570794, 37.25130442578125],
        [57.858406154570794, 97.25130442578125]
    ]
    test_response_data = [
        {
            "success": True,
            "code": 200,
            "message": None,
            "data": [{
                'id': couriers[2].id,
                'first_name': couriers[2].first_name,
                'last_name': couriers[2].last_name,
                'email': couriers[2].email,
                'phone': couriers[2].phone,
                'date_joined': (
                    couriers[2].date_joined - timedelta(hours=offset)
                ).isoformat() + '+00:00',
                'coordinates': couriers[2].coordinates,
                'delivery_zone': (lambda dz: {'id': dz.id, 'name': dz.name})(
                    await couriers[2].delivery_zone
                )
            }]
        },
        {
            "success": False,
            "code": 422,
            "message": "Delivery area not found!",
            "data": None
        }
    ]
    for index, data in enumerate(test_request_data):
        response = await cli.get(
            f"/api/search/courier?longitude={data[0]}&latitude={data[1]}",
        )
        assert response.status == test_response_data[index]['code']
        response = await response.json()
        assert response == test_response_data[index]
