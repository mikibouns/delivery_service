"""testing module"""
from datetime import timedelta

from utils import helper_lib as hl


async def test_get_courier(cli, couriers):
    """
    Test get courier
    :param cli: client
    :param couriers: couriers
    :return:
    """
    offset = hl.get_timezone_offset()
    test_response_data = [{
        'id': data.id,
        'first_name': data.first_name,
        'last_name': data.last_name,
        'email': data.email,
        'phone': data.phone,
        'date_joined': (
            data.date_joined - timedelta(hours=offset)
        ).isoformat() + '+00:00',
        'coordinates': data.coordinates,
        'delivery_zone': (lambda dz: {'id': dz.id, 'name': dz.name})(
            await data.delivery_zone
        )
    } for data in couriers]
    response = await cli.get("/api/couriers")
    assert response.status == 200
    response = await response.json()
    assert response['data'] == test_response_data


async def test_set_courier(cli, delivery_zones):
    """
    Test set courier
    :param cli: client
    :param delivery_zones: delivery zones
    :return:
    """
    test_request_data = [
        [{
            "first_name": "some_first_name",
            "last_name": "some_last_name",
            "email": "some_user@example.com",
            "phone": "89527810522",
            "coordinates": [
                55.8251794700682,
                36.91484812695314
            ],
            "delivery_zone_id": delivery_zones[0].id
        }],
        [{
            "first_name": "some_first_name",
            "last_name": "some_last_name",
            "email": "some_user@example.com",
            "phone": "89527810522",
            "coordinates": [
                55.8251794700682,
                36.91484812695314
            ],
            "delivery_zone_id": 999
        }],
        [{
            "first_name": "some_first_name",
            "last_name": "some_last_name",
            "coordinates": [
                55.8251794700682,
                36.91484812695314
            ],
            "delivery_zone_id": 999
        }],
        [{
            "first_name": "some_first_name",
            "last_name": "some_last_name",
            "email": 1234,
            "phone": "89527810522",
            "coordinates": [
                55.8251794700682,
                36.91484812695314
            ],
            "delivery_zone_id": 999
        }],
        [{
            "first_name": "some_first_name",
            "last_name": "some_last_name",
            "email": "some_user@example.com",
            "phone": "89527810522",
            "coordinates": [
                55.8251794700682
            ],
            "delivery_zone_id": 999
        }]
    ]
    test_response_data = [
        {
            "success": True,
            "code": 200,
            "message": None,
            "data": [
                {
                    "first_name": "some_first_name",
                    "last_name": "some_last_name",
                    "email": "some_user@example.com",
                    "phone": "89527810522",
                    "coordinates": [
                        55.8251794700682,
                        36.91484812695314
                    ],
                    "delivery_zone": {
                        "id": delivery_zones[0].id,
                        "name": delivery_zones[0].name
                    }
                }
            ]
        },
        {
            "success": False,
            "code": 422,
            "message": "Delivery zone ID 999 does not exist!",
            "data": None
        },
        {
            "success": False,
            "code": 400,
            "message": {
                "body": {"0": {
                    "phone": "required property", "email": "required property"
                }}
            },
            "data": None
        },
        {
            "success": False,
            "code": 400,
            "message": {
                "body": {"0": {"email": "value should be type of str"}}
            },
            "data": None
        },
        {
            "success": False,
            "code": 422,
            "message": {
                "body": {"coordinates": "The number of elements should be 2"}
            },
            "data": None
        }
    ]
    for index, data in enumerate(test_request_data):
        response = await cli.post("/api/couriers", json=data)
        assert response.status == test_response_data[index]['code']
        response = await response.json()
        if response['code'] == 200:
            response['data'][0].pop('id')
            response['data'][0].pop('date_joined')

        assert response == test_response_data[index]


async def test_update_courier(cli, couriers):
    """
    Test update courier
    :param cli: client
    :param couriers: couriers
    :return:
    """
    offset = hl.get_timezone_offset()
    test_request_data = [
        {
            'couriers': couriers[0].id,
            'data': {"delivery_zone_id": couriers[0].delivery_zone_id + 1}
        },
        {
            'couriers': 999,
            'data': {}
        },
        {
            'couriers': couriers[0].id,
            'data': {"coordinates": [55.8251794700682]}
        },
        {
            'couriers': couriers[0].id,
            'data': {"email": "test_email"}
        }
    ]
    test_response_data = [
        {
            "success": True,
            "code": 200,
            "message": None,
            "data": [{
                'id': couriers[0].id,
                'first_name': couriers[0].first_name,
                'last_name': couriers[0].last_name,
                'email': couriers[0].email,
                'phone': couriers[0].phone,
                'date_joined': (
                   couriers[0].date_joined - timedelta(hours=offset)
                ).isoformat() + '+00:00',
                'coordinates': couriers[0].coordinates,
                'delivery_zone': (
                    lambda dz: {'id': dz.id + 1, 'name': 'zone_2'}
                )(await couriers[0].delivery_zone)
            }]
        },
        {
            "success": False,
            "code": 404,
            "message": "Not Found",
            "data": None
        },
        {
            "success": False,
            "code": 422,
            "message": {
                "body": {"coordinates": "The number of elements should be 2"}
            },
            "data": None
        },
        {
            "success": False,
            "code": 400,
            "message": {"body": {"email": "value should be valid email"}},
            "data": None
        }
    ]
    for index, data in enumerate(test_request_data):
        response = await cli.patch(
            f"/api/couriers/{data['couriers']}", json=data['data']
        )
        assert response.status == test_response_data[index]['code']
        response = await response.json()
        assert response == test_response_data[index]
