"""config for testing module"""
from aiohttp import web
import pytest
from aiohttp_swagger3 import SwaggerDocs, SwaggerUiSettings


import views
from middlewares import middleware_handler
from db.init_db import db_engine
from db import models


@pytest.fixture
def cli(loop, aiohttp_client):
    """
    Aiohttp client for testing
    :param loop: async loop
    :param aiohttp_client: aiohttp client
    :return:
    """
    app = web.Application(loop=loop, middlewares=[middleware_handler])
    swagger = SwaggerDocs(
        app,
        swagger_ui_settings=SwaggerUiSettings(path="/api/docs/"),
        components="components.yml"
    )
    swagger.add_routes(views.routes)
    db_engine(app)
    return loop.run_until_complete(aiohttp_client(app))


@pytest.fixture
async def delivery_zones():
    """
    Creating delivery zones
    :return: delivery zones
    """
    delivery_zone_list = [{
        'name': f'zone_{idx + 1}', 'coordinates': coordinates
    } for idx, coordinates in enumerate([
        [
            [55.8251794700682, 36.91484812695314],
            [55.79037681251229, 37.37078074414063],
            [55.91012114854278, 37.57402781445313],
            [56.05639958730052, 37.43532542187501],
            [56.03949052052108, 37.00823191601564],
            [55.8251794700682, 36.91484812695314]
        ],
        [
            [56.06006621592205, 37.4335901427101],
            [56.18969818710818, 37.583278863413206],
            [56.084645590749574, 38.006252496225706],
            [55.821137658614745, 37.837337701303824],
            [55.91225914962281, 37.576412408335095],
            [56.06006621592205, 37.4335901427101]
        ],
        [
            [55.78923308820743, 37.36732885120619],
            [55.91013799830256, 37.576069085581175],
            [55.821330939602774, 37.83699437854993],
            [55.65277587544564, 37.841719988961714],
            [55.59371854351092, 37.510756854196096],
            [55.78923308820743, 37.36732885120619]
        ]
    ])]
    delivery_zone_obj_list = []
    for data in delivery_zone_list:
        dz_obj, _ = await models.DeliveryZone.get_or_create(**data)
        delivery_zone_obj_list.append(dz_obj)

    yield delivery_zone_obj_list

    await models.Courier.all().delete()
    await models.DeliveryZone.all().delete()


@pytest.fixture
async def couriers(delivery_zones):
    """
    Creating couriers
    :param delivery_zones: delivery zones
    :return: couriers
    """
    courier_list = [{
        'first_name': f'FirstCourierName{idx}',
        'last_name': f'LastCourierName{idx}',
        'email': f'courier{idx}@gmail.com',
        'phone': f'8952791094{idx}',
        'is_active': True,
        'coordinates': coordinates,
        'delivery_zone': delivery_zone
    } for idx, delivery_zone, coordinates in [
        (1, delivery_zones[0], [55.91434461155746, 37.3198694030242]),
        (2, delivery_zones[0], [55.939017200935496, 37.247084979196096]),
        (3, delivery_zones[0], [55.84950398532422, 37.17292726435234]),
        (4, delivery_zones[1], [55.89505817364596, 37.7098840514617]),
        (5, delivery_zones[2], [55.66597384054886, 37.58216798700858])
    ]]
    courier_obj_list = []
    for data in courier_list:
        dz_obj, _ = await models.Courier.get_or_create(**data)
        courier_obj_list.append(dz_obj)

    yield courier_obj_list

    await models.Courier.all().delete()
