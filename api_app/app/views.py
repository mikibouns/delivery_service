"""module for application's views"""
from aiohttp import web
from tortoise.exceptions import IntegrityError

from handlers import search_handler as sh
from db import models
from db import pd_models as pm


routes = web.RouteTableDef()


@routes.get("/api/delivery-zones", allow_head=False)
async def get_delivery_zones() -> web.json_response:
    """
    ---
    tags:
      - Delivery zones
    responses:
      '200':
        description: Get delivery zones
        content:
          application/json:
            schema:
              type: object
              properties:
                success:
                  type: boolean
                  example: true
                code:
                  type: integer
                  example: 200
                message:
                  type: string
                  example: null
                data:
                  $ref: "#/components/schemas/delivery_zones_response"
    """
    response = await models.DeliveryZone.filter(
        is_active=True
    ).values('id', 'name', 'coordinates')
    return web.json_response(response)


@routes.post("/api/delivery-zones")
async def set_delivery_zones(request: web.Request) -> web.json_response:
    """
    ---
    tags:
      - Delivery zones
    requestBody:
      description: Set delivery zones
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/delivery_zones_request"
    responses:
      '200':
        description: Set delivery zones
        content:
          application/json:
            schema:
              type: object
              properties:
                success:
                  type: boolean
                  example: true
                code:
                  type: integer
                  example: 200
                message:
                  type: string
                  example: null
                data:
                  $ref: "#/components/schemas/delivery_zones_response"
    """
    request_data = await request.json()
    response = []
    for delivery_zone in request_data:
        pm.DeliveryZonesRequestModel(**delivery_zone)
        try:
            delivery_zone_obj = await models.DeliveryZone.filter(
                **delivery_zone
            )
            if not delivery_zone_obj:
                delivery_zone_obj = await models.DeliveryZone.create(
                    **delivery_zone
                )
            else:
                delivery_zone_obj = delivery_zone_obj[0]
        except IntegrityError as ex:
            raise web.HTTPUnprocessableEntity(reason=str(ex))
        else:
            response.append({
                'id': delivery_zone_obj.id,
                'name': delivery_zone_obj.name,
                'coordinates': delivery_zone_obj.coordinates
            })
    return web.json_response(response)


@routes.get("/api/couriers", allow_head=False)
async def get_couriers() -> web.json_response:
    """
    ---
    tags:
      - Couriers
    responses:
      '200':
        description: Get couriers
        content:
          application/json:
            schema:
              type: object
              properties:
                success:
                  type: boolean
                  example: true
                code:
                  type: integer
                  example: 200
                message:
                  type: string
                  example: null
                data:
                  $ref: "#/components/schemas/get_couriers_response"
    """
    response = await models.Courier.get_courier_list()
    return web.json_response(response)


@routes.post("/api/couriers")
async def set_couriers(request: web.Request) -> web.json_response:
    """
    ---
    tags:
      - Couriers
    requestBody:
      description: Set couriers
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/couriers_request"
    responses:
      '200':
        description: Set couriers
        content:
          application/json:
            schema:
              type: object
              properties:
                success:
                  type: boolean
                  example: true
                code:
                  type: integer
                  example: 200
                message:
                  type: string
                  example: null
                data:
                  $ref: "#/components/schemas/get_couriers_response"
    """
    request_data = await request.json()
    response = []
    couriers_ids = []
    for courier in request_data:
        pm_courier = pm.CouriersRequestModel(**courier)
        if not await models.DeliveryZone.exists(
                id=pm_courier.delivery_zone_id
        ):
            raise web.HTTPUnprocessableEntity(
                reason=f'Delivery zone ID {pm_courier.delivery_zone_id} '
                       f'does not exist!'
            )
        try:
            couriers_obj = await models.Courier.filter(**courier)
            if not couriers_obj:
                couriers_obj = await models.Courier.create(**courier)
            else:
                couriers_obj = couriers_obj[0]
        except IntegrityError as ex:
            raise web.HTTPUnprocessableEntity(reason=str(ex))
        else:
            couriers_ids.append(couriers_obj.id)

        response = await models.Courier.get_courier_list(tuple(couriers_ids))
    return web.json_response(response)


@routes.patch("/api/couriers/{id}")
async def update_couriers(request: web.Request) -> web.json_response:
    """
    ---
    tags:
      - Couriers
    parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
    requestBody:
      description: Update couriers
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/update_couriers_request"
    responses:
      '200':
        description: Update couriers
        content:
          application/json:
            schema:
              type: object
              properties:
                success:
                  type: boolean
                  example: true
                code:
                  type: integer
                  example: 200
                message:
                  type: string
                  example: null
                data:
                  $ref: "#/components/schemas/get_couriers_response"
    """
    couriers_id = request.match_info.get('id')
    courier_obj = await models.Courier.get_or_none(id=couriers_id).values()
    if not courier_obj:
        raise web.HTTPNotFound()

    request_data = await request.json()
    courier_obj.update(request_data)
    if request_data.get('delivery_zone_id') and \
            not await models.DeliveryZone.exists(
                id=request_data['delivery_zone_id']
            ):
        raise web.HTTPUnprocessableEntity(
            reason=f'Delivery zone ID {request_data["delivery_zone_id"]} '
                   f'does not exist!'
        )
    pm_courier = pm.CouriersRequestModel(**courier_obj)
    await models.Courier.filter(id=couriers_id).update(**pm_courier.dict())
    response = await models.Courier.get_courier_list((couriers_id, ))
    return web.json_response(response)


@routes.get("/api/search/courier", allow_head=False)
async def search_courier(request: web.Request) -> web.json_response:
    """
    ---
    tags:
      - Searcher
    parameters:
      - name: longitude
        in: query
        required: true
        schema:
          type: number
          example: 55.858406154570794
      - name: latitude
        in: query
        required: true
        schema:
          type: number
          example: 37.25130442578125
    responses:
      '200':
        description: Search couriers
        content:
          application/json:
            schema:
              type: object
              properties:
                success:
                  type: boolean
                  example: true
                code:
                  type: integer
                  example: 200
                message:
                  type: string
                  example: null
                data:
                  $ref: "#/components/schemas/get_couriers_response"
    """
    longitude = float(request.query.get('longitude'))
    latitude = float(request.query.get('latitude'))
    dz_obj = await sh.get_delivery_zone([longitude, latitude])
    if not dz_obj:
        raise web.HTTPUnprocessableEntity(reason='Delivery area not found!')
    courier_obj = await sh.find_courier([longitude, latitude], dz_obj.id)
    if not courier_obj:
        raise web.HTTPUnprocessableEntity(reason='Courier not found!')
    response = await models.Courier.get_courier_list((courier_obj.id, ))
    return web.json_response(response)
