"""utility module"""
from datetime import datetime, timedelta, timezone
from math import sin, cos, sqrt, atan2, radians
from typing import List

from shapely.geometry import Point
from shapely.geometry.polygon import Polygon


async def formatted_pydantic_error(error_list: list) -> dict:
    """
    Formatted pydantic error
    :param error_list: error list
    :return: formatted error
    """
    counts = {}
    for err in error_list:
        parts = err['loc']
        branch = counts
        for item, part in enumerate(parts):
            if item == len(parts) - 1:
                break
            branch = branch.setdefault(part, {})
        branch[parts[-1]] = err['msg']
    return counts


def get_timezone_offset() -> int:
    """
    Get timezone offset
    :return: timezone offset
    """
    local_time = datetime.now(timezone.utc).astimezone()
    return (local_time.utcoffset() // timedelta(seconds=1)) // (60 * 60)


def coordinate_distance_calculator(coordinate_list: list) -> float:
    """
    Coordinate distance calculator
    :param coordinate_list: first coordinates,
    example: [[20.510138, 54.710163], [20.510138, 54.710163]]
    :return: distance
    """
    R = 6373.0
    lat1 = radians(coordinate_list[0][0])
    lon1 = radians(coordinate_list[0][1])
    lat2 = radians(coordinate_list[1][0])
    lon2 = radians(coordinate_list[1][1])

    dlon = lon2 - lon1
    dlat = lat2 - lat1
    value_a = (sin(dlat / 2)) ** 2 + cos(lat1) * cos(lat2) * (sin(dlon / 2)) ** 2
    value_c = 2 * atan2(sqrt(value_a), sqrt(1 - value_a))
    distance = R * value_c
    return distance


def is_point_in_area(point: List[float], area: List[list[float]]) -> bool:
    """
    Checks if a point is in an area
    :param point: example: [55.858406154570794, 37.25130442578125]
    :param area: example: [
        [55.8251794700682, 36.91484812695314],
        [55.79037681251229, 37.37078074414063],
        [55.91012114854278, 37.57402781445313]
    ]
    :return: boolean
    """
    point = Point(*point)
    polygon = Polygon(area)
    return polygon.contains(point)
