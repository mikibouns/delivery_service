import os
import json


# API
DEBUG = bool(int(os.getenv('DEBUG', '1')))
API_VERSION = os.getenv('API_VERSION', '0.0.1')
APP_PORT = os.getenv('APP_PORT', '8080')
MAIN_HOST = os.getenv('MAIN_HOST', '127.0.0.1')

# CORS
CORS_ALLOW_ORIGINS = json.loads(os.getenv('CORS_ALLOW_ORIGINS', '[]'))
CORS_ALLOW_ALL = bool(int(os.getenv('CORS_ALLOW_ALL', '1')))

# postgresql config
DB_HOST = os.getenv('DB_HOST', '127.0.0.1')
DB_PORT = int(os.getenv('DB_PORT', '5438'))
DB_NAME = os.getenv('DB_NAME', 'ds')
DB_USER = os.getenv('DB_USER', 'ds')
DB_PASS = os.getenv('DB_PASS', 'test_password')
