import multiprocessing


bind = '0.0.0.0:80'
backlog = 2048
capture_output = True
loglevel = "info"
workers = multiprocessing.cpu_count() * 2 + 1
worker_class = 'gevent'
worker_connections = 1000
timeout = 300
keepalive = 2
