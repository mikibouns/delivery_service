#!/bin/bash

echo "MAKING MIGRATE"
python manage.py migrate
python manage.py fill_test_data

echo "RUN APP"
gunicorn delivery_service.wsgi:application -c gunicorn.conf.py
