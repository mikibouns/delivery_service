from django.contrib import admin

from . import models


class ClientAdmin(admin.ModelAdmin):
    fields = [
        'id', 'first_name', 'last_name', 'email',
        'phone', 'is_active', 'date_joined'
    ]
    list_display = ['email', 'first_name', 'last_name', 'date_joined']
    search_fields = ['email', 'first_name', 'last_name', 'date_joined']
    readonly_fields = ['id', 'date_joined']


class CourierAdmin(admin.ModelAdmin):
    fields = [
        'id', 'is_active', 'first_name', 'last_name', 'email',
        'phone', 'delivery_zone', 'coordinates', 'date_joined'
    ]
    list_display = ['email', 'first_name', 'last_name', 'date_joined']
    search_fields = ['email', 'first_name', 'last_name', 'date_joined']
    readonly_fields = ['id', 'date_joined', 'coordinates']


admin.site.register(models.Client, ClientAdmin)
admin.site.register(models.Courier, CourierAdmin)
admin.site.register(models.DeliveryZone)
admin.site.register(models.Order)

admin.site.site_header = "Delivery service"
admin.site.site_title = "Delivery service"
admin.site.index_title = "Welcome to Delivery service Admin Portal"
