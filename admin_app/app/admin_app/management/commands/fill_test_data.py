from loguru import logger
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

from admin_app import models


User = get_user_model()


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.create_superuser()
        self.create_delivery_zones()
        self.create_couriers()

    def create_handler(self, model, data_list: list, model_name: str) -> list:
        """
        Create test data handler
        :param model: BD Model
        :param data_list: test data list
        :param model_name: model name for logs
        :return: list
        """
        object_list = []
        for data in data_list:
            try:
                obj, status = model.objects.get_or_create(**data)
                if not status:
                    logger.warning(f"{model_name}: {obj} already created!")
                else:
                    logger.info(f"{model_name}: {obj} is created")
            except Exception as ex:
                logger.error(ex)
                raise
            else:
                object_list.append(obj)
        return object_list

    def create_superuser(self):
        username = 'admin'
        password = 'admin_password'
        email = 'admin@mail.com'

        if User.objects.filter(username=username).exists():
            logger.warning('Superuser already created!')
        else:
            try:
                User.objects.create_superuser(username=username, email=email, password=password)
            except Exception as ex:
                logger.error(ex)
                raise
            else:
                logger.info(f"""
                --------------------
                | CREATE SUPERUSER |    
                --------------------
                Username: {username}
                Password: {password}
                Email: {email}
                --------------------
                """)

    def create_delivery_zones(self):
        delivery_zone_list = [{
            'name': f'zone_{idx + 1}', 'coordinates': coordinates
        } for idx, coordinates in enumerate([
            [
                [55.8251794700682, 36.91484812695314],
                [55.79037681251229, 37.37078074414063],
                [55.91012114854278, 37.57402781445313],
                [56.05639958730052, 37.43532542187501],
                [56.03949052052108, 37.00823191601564],
                [55.8251794700682, 36.91484812695314]
            ],
            [
                [56.06006621592205, 37.4335901427101],
                [56.18969818710818, 37.583278863413206],
                [56.084645590749574, 38.006252496225706],
                [55.821137658614745, 37.837337701303824],
                [55.91225914962281, 37.576412408335095],
                [56.06006621592205, 37.4335901427101]
            ],
            [
                [55.78923308820743, 37.36732885120619],
                [55.91013799830256, 37.576069085581175],
                [55.821330939602774, 37.83699437854993],
                [55.65277587544564, 37.841719988961714],
                [55.59371854351092, 37.510756854196096],
                [55.78923308820743, 37.36732885120619]
            ]
        ])]
        self.delivery_zone_list = self.create_handler(
            models.DeliveryZone, delivery_zone_list, 'DeliveryZone'
        )

    def create_couriers(self):
        courier_list = [{
            'first_name': f'FirstCourierName{idx}',
            'last_name': f'LastCourierName{idx}',
            'email': f'courier{idx}@gmail.com',
            'phone': f'8952791094{idx}',
            'is_active': True,
            'coordinates': coordinates,
            'delivery_zone': delivery_zone
        } for idx, delivery_zone, coordinates in [
            (1, self.delivery_zone_list[0], [55.91434461155746, 37.3198694030242]),
            (2, self.delivery_zone_list[0], [55.939017200935496, 37.247084979196096]),
            (3, self.delivery_zone_list[0], [55.84950398532422, 37.17292726435234]),
            (4, self.delivery_zone_list[1], [55.89505817364596, 37.7098840514617]),
            (5, self.delivery_zone_list[2], [55.66597384054886, 37.58216798700858])
        ]]
        self.courier_list = self.create_handler(
            models.Courier, courier_list, 'Courier'
        )
