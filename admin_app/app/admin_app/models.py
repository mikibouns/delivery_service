from datetime import datetime

from django.db import models
from jsoneditor.fields.django3_jsonfield import JSONField


class DeliveryZone(models.Model):
    name = models.CharField(verbose_name='Name', max_length=254, unique=True)
    coordinates = JSONField(verbose_name='Coordinates')
    is_active = models.BooleanField(verbose_name='Is active', default=True)

    class Meta:
        db_table = 'delivery_zone'

    def __str__(self):
        return f'{self.name}'


class Courier(models.Model):
    first_name = models.CharField(verbose_name='First name', max_length=254, blank=True, null=True)
    last_name = models.CharField(verbose_name='Last name', max_length=254, blank=True, null=True)
    email = models.CharField(verbose_name='Email', max_length=254, unique=True)
    phone = models.CharField(verbose_name='Phone', max_length=128, blank=True, null=True)
    is_active = models.BooleanField(verbose_name='Is active', default=True)
    date_joined = models.DateTimeField(verbose_name='Date joined', default=datetime.now)
    coordinates = JSONField(verbose_name='Coordinates', blank=True, null=True)
    delivery_zone = models.ForeignKey(
        DeliveryZone, verbose_name='Delivery zone', on_delete=models.SET_NULL, related_name="courier",
        blank=True, null=True, default=None
    )

    class Meta:
        db_table = 'courier'
        ordering = ['date_joined']

    def __str__(self):
        return f'{self.email}'


class Client(models.Model):
    first_name = models.CharField(verbose_name='First name', max_length=254, blank=True, null=True)
    last_name = models.CharField(verbose_name='Last name', max_length=254, blank=True, null=True)
    email = models.CharField(verbose_name='Email', max_length=254, unique=True)
    phone = models.CharField(verbose_name='Phone', max_length=128, blank=True, null=True)
    is_active = models.BooleanField(verbose_name='Is active', default=True)
    date_joined = models.DateTimeField(verbose_name='Date joined', default=datetime.now)

    class Meta:
        db_table = 'client'
        ordering = ['date_joined']

    def __str__(self):
        return f'{self.email}'


class Order(models.Model):
    STATUS_CHOICES = (
        (0, 'waiting for courier'),
        (1, 'order accepted'),
        (2, 'delivery'),
        (3, 'delivered'),
        (4, 'not delivered')
    )

    title = models.CharField(verbose_name='Title', max_length=254, blank=True, null=True)
    description = models.TextField(verbose_name='Description', blank=True, null=True)
    quantity = models.PositiveIntegerField(verbose_name='Quantity', default=0)
    price = models.FloatField(verbose_name='Price', default=0)
    address = models.CharField(verbose_name='Address', max_length=254, blank=True, null=True)
    coordinates = JSONField(verbose_name='Coordinates', blank=True, null=True)
    client = models.ForeignKey(Client, verbose_name='Client', on_delete=models.CASCADE, related_name="client_order")
    courier = models.ForeignKey(
        Courier, verbose_name='Courier', on_delete=models.CASCADE, related_name="courier_order", blank=True, null=True
    )
    status = models.PositiveSmallIntegerField(verbose_name='Order status', choices=STATUS_CHOICES, default=0)

    class Meta:
        db_table = 'order'

    def __str__(self):
        return f'{self.title}'
